<?php

class User
{
    public $id;
    public $email;
    public $password;

    public function validate_registration($array)
    {
        $bd = bd();

        $error = [];

        $array['password'] = strip_tags($array['password']);
        $array['email'] = strip_tags($array['email']);

        if (empty($array['password'])) {
            $error[] = 'Введите пароль.';
        } else {
            if (strlen($array['password']) < 3) {
                $error[] = 'Пароль слишком короткий.';
            }

            if (empty($array['passwordConfirm'])) {
                $error[] = 'Введите копию пароля.';
            }

            if ($array['password'] != $array['passwordConfirm']) {
                $error[] = 'Пароли не совпадают.';
            }
        }

        if (!filter_var($array['email'], FILTER_VALIDATE_EMAIL)) {
            $error[] = 'Email невалидный';
        } else {
            $stmt = $bd->prepare('SELECT email FROM users WHERE email = :email');
            $stmt->execute(array(':email' => $array['email']));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            if (!empty($row['email'])) {
                $error[] = 'Введённый email уже существует.';
            }
        }

        $_SESSION['errors'] = $error;

        $bd = null;
    }

    public function validate_login_and_load($array)
    {
        $bd = bd();

        $error = [];

        $array['password'] = strip_tags($array['password']);
        $array['email'] = strip_tags($array['email']);

        if (empty($array['password'])) {
            $error[] = 'Введите пароль.';
        }

        if (!filter_var($array['email'], FILTER_VALIDATE_EMAIL)) {
            $error[] = 'Email невалидный';
        }

        $stmt = $bd->prepare("SELECT * FROM users WHERE email=:email");
        $stmt->execute(['email' => $array['email']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (empty($row['email']) || password_verify($array['password'], $row['password'])==false) {
            $error[] = 'Вы неправильно ввели пароль или email.';
        }

        $_SESSION['errors'] = $error;

        if (!empty($row['id'])){
            $this->id = $row['id'];
            $this->email = $row['email'];
            $this->password = $row['password'];
        }

        $bd = null;
    }

    public function create($array)
    {
        $bd = bd();

        $password = password_hash($array['password'], PASSWORD_DEFAULT);

        $stmt = $bd->prepare('INSERT INTO users (email,password) VALUES (:email, :password)');
        $stmt->execute(array(
            ':email' => $array['email'],
            ':password' => $password
        ));

        $this->id = $bd->lastInsertId('id');
        $this->email = $array['email'];
        $this->password = $password;

        $bd = null;
    }
}
