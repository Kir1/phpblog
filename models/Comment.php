<?php

class Comment
{
    public function validate($array)
    {
        $error = [];

        if (empty($array['author'])) {
            $error[] = 'Введите своё имя.';
        } elseif (strlen($array['author']) > 200)
            $error[] = 'Имя слишком длинное.';

        if (empty($array['text'])) {
            $error[] = 'Введите текст.';
        } elseif (strlen($array['text']) > 65536)
            $error[] = 'Текст слишком большой.';

        $_SESSION['errors'] = $error;
    }

    public function create($array)
    {
        $bd = bd();

        $stmt = $bd->prepare('INSERT INTO comments (author,text,article_id) VALUES (:author, :text, :article_id)');
        $stmt->execute(array(
            ':author' => $array['author'],
            ':text' => $array['text'],
            ':article_id' => $array['article_id']
        ));

        $id = $bd->lastInsertId('id');

        $bd = null;

        return $id;
    }
}
