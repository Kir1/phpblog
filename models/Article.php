<?php

class Article
{
    public function load($id)
    {
        $bd = bd();
        $stmt = $bd->prepare("SELECT id, title, created, text FROM articles WHERE id=:id");
        $stmt->execute(['id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $bd = null;
        if (empty($row['id'])) return null;
        return $row;
    }

    public function comments($id)
    {
        $bd = bd();
        $stmt = $bd->prepare("SELECT id,author,text FROM comments WHERE article_id=:article_id");
        $stmt->execute(['article_id' => $id]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $bd = null;
        return $rows;
    }

    public function all()
    {
        $bd = bd();

        $GLOBALS["paginathion_link"] = url('articles');
        $GLOBALS["paginathion_total"] = $bd->query('SELECT COUNT(*) FROM articles')->fetchColumn();
        $GLOBALS["paginathion_limit"] = 5;
        $GLOBALS["paginathion_pages"] = ceil($GLOBALS["paginathion_total"] / $GLOBALS["paginathion_limit"]);
        $GLOBALS["paginathion_page"] = min( $GLOBALS["paginathion_pages"], filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
            'options' => array(
                'default'   => 1,
                'min_range' => 1,
            ),
        )));
        $GLOBALS["paginathion_offset"]  = ($GLOBALS["paginathion_page"] - 1)  * $GLOBALS["paginathion_limit"];
        $GLOBALS["paginathion_start"]= $GLOBALS["paginathion_offset"] + 1;
        $GLOBALS["paginathion_end"] = min(($GLOBALS["paginathion_offset"] + $GLOBALS["paginathion_limit"]), $GLOBALS["paginathion_total"]);

        $stmt = $bd->prepare("SELECT id,preview,title,created FROM articles ORDER BY created DESC LIMIT :limit OFFSET :offset"); //
        $stmt->bindParam(':limit', $GLOBALS["paginathion_limit"], PDO::PARAM_INT);
        $stmt->bindParam(':offset', $GLOBALS["paginathion_offset"], PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $bd = null;

        if ($rows) return $rows;

        return null;
    }
}
