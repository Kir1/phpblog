<?php

class CommentController
{
    function __construct()
    {
        anonymus_forbidden();

        if ($GLOBALS["request_post"]) $this->create();
    }

    protected function create()
    {
        include_once 'models/Comment.php';
        $comment = new Comment();

        $comment->validate($_POST);
        $url_redirect = 'articles/'.$_POST['article_id'];
        if (check_errors()) {
            $_SESSION['old_data'] = $_POST;
            redirect($url_redirect,'form_create_comment');
        } else {
            $id = $comment->create($_POST);
            redirect($url_redirect,"comment$id");
        }
    }
}