<?php

class AuthorizationController
{
    function __construct()
    {
        if ($GLOBALS["request_post"]) {
            user_forbidden();
            $this->postLogin();
        }
        else {
            switch ($GLOBALS["url_last"]) {
                case "login":
                    user_forbidden();
                    $this->getLogin();
                    break;
                case "logout":
                    $this->postLogout();
                    break;
            }
        }
    }

    protected function getLogin()
    {
        page_ini('Авторизация', 'main/layout', 'page/user/authorization');
    }

    protected function postLogin()
    {
        $user = new User();

        $user->validate_login_and_load($_POST);
        if (check_errors()) {
            $_SESSION['old_data'] = $_POST;
            reload();
        } else {
            login($user);
            redirect();
        }
    }

    protected function postLogout()
    {
        logout();
    }
}