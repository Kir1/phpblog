<?php

class RegistrationController
{
    function __construct()
    {
        user_forbidden();

        if ($GLOBALS["request_post"])
            $this->postRegister();
        else
            $this->getRegister();
    }

    protected function getRegister()
    {
        page_ini('Регистрация', 'main/layout', 'page/user/registration');
    }

    protected function postRegister()
    {
        $user = new User();
        $user->validate_registration($_POST);
        if (check_errors()) {
            $_SESSION['old_data'] = $_POST;
            reload();
        } else {
            $user->create($_POST);
            login($user);
            redirect();
        }
    }
}