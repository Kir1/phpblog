<?php

class ArticleController
{
    function __construct()
    {
        include_once 'models/Article.php';
        $particles_page = ($GLOBALS["url_last"] == 'articles');
        if ($particles_page || $GLOBALS["url_last"] == '') $this->index();
        elseif (is_int((int)$GLOBALS["url_last"])) $this->show();
    }

    public function index()
    {
        $articles = new Article();
        $GLOBALS["articles"] =  $articles->all();
        page_ini('Статьи', 'main/layout', 'page/article/index');
    }

    public function show()
    {
        $article = new Article();
        $GLOBALS["comments"] = $article->comments($GLOBALS["url_last"]);
        $article = $article->load($GLOBALS["url_last"]);
        $GLOBALS["article"] = $article;
        if ($article) page_ini( $article['title'], 'main/layout', 'page/article/show');
        else error404();
    }
}
