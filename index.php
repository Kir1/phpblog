<?php
include_once 'global/main.php';
include_once 'models/User.php'; //для инициализации в сессии, доступности свойств и методов

session_start();
if (!isset($_SESSION['errors'])) {
    $_SESSION['errors'] = "";
}
if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = null;
} else {
    if ($_SESSION['user']) {
        $bd = bd();
        $stmt = $bd->prepare("SELECT id FROM users WHERE id=:id AND email=:email AND password=:password");
        $stmt->execute(['id' => $_SESSION['user']->id, 'email' => $_SESSION['user']->email, 'password' => $_SESSION['user']->password]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (empty($row['id'])) logout();
    }
}
if (!isset($_SESSION['old_data'])) {
    $_SESSION['old_data'] = null;
}

$url = $_SERVER['REDIRECT_URL'];
$url_array = explode("/", $url);
$url_first = $url_array[1];
$url_last = $url_array[count($url_array) - 1];

$request_post = false;
if ($_SERVER['REQUEST_METHOD'] === 'POST') $request_post = true;

if ($url == "/login" || $url == "/logout") {
    load_controller("AuthorizationController");
} elseif ($url == "/registration") {
    load_controller("RegistrationController");
} elseif ($url_first == "comments") {
    load_controller("CommentController");
}
elseif ($url == "" || $url_first == 'articles') {
    load_controller("ArticleController");
} else
    error404();