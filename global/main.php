<?php
//file
function file_get_real_path($file_path = '')
{
    return $_SERVER["DOCUMENT_ROOT"] . "/$file_path";
}

//page
function page_ini($title, $layout, $child_view)
{
    $GLOBALS['title'] = $title;
    $GLOBALS['child_view'] = $child_view;
    ob_start();
    view_rendering($layout);
    $content = ob_get_contents();
    ob_end_clean();
    echo $content;
    $_SESSION['errors'] = "";
    $_SESSION['old_data'] = null;
    exit();
}

//view
function view_rendering($view_path = '')
{
    include file_get_real_path('views/' . $view_path . '.php');
}

//errors
function check_errors()
{
    return count($_SESSION['errors']) > 0;
}

///url
function url_domain()
{
    return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
}

function url_relative_page()
{
    return $_SERVER[REQUEST_URI];
}

function url_absolute_page()
{
    return url_domain() . url_relative_page();
}

function url($url = '')
{
    return url_domain() . '/' . $url;
}

//redirect
function reload($anchor = "")
{
    if ($anchor) $anchor = "#$anchor ";
    http_response_code(303);
    header("Location: " . $_SERVER['REQUEST_URI'] . $anchor);
    exit();
}

function redirect($url = '', $anchor = "")
{
    if ($anchor) $anchor = "#$anchor ";
    http_response_code(303);
    header("Location: " . url($url) . $anchor);
    exit();
}

//errors
function error404()
{
    header("HTTP/1.0 404 Not Found");
    page_ini('Ошибка 404', 'main/layout','error/404');
}

function error403()
{
    header("HTTP/1.0 403 Forbidden");
    page_ini('Ошибка 403', 'main/layout', 'error/403');
}

//denied
function user_forbidden()
{
    if (check_auth()) error403();
}

function anonymus_forbidden()
{
    if (check_auth() == false) error403();
}

//basedate
function bd()
{
    try {
        $dbh = new PDO('mysql:host=localhost;dbname=phpblog', 'root', '');
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }

    return $dbh;
}

//auth
function check_auth()
{
    if ($_SESSION['user']) return true;
    return false;
}

//controllers
function load_controller($name)
{
    include_once "controllers/$name.php";
    new $name;
}

//form
function form_old_value($field)
{
    if (isset($_SESSION['old_data'][$field])) echo $_SESSION['old_data'][$field];
}

//user
function login($user)
{
    $_SESSION['user'] = $user;
    redirect();
}

function logout()
{
    $_SESSION['user'] = null;
    redirect();
}