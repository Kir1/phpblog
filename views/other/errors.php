<?php
if ($_SESSION['errors']) {
    ?>
    <div class="row mt-5 mb-2 p-3 border border-danger bg-danger text-white rounded">
        <div class="col">
            <h4>Ошибки:</h4>
            <?php
            foreach ($_SESSION['errors'] as $error) {
                ?>
                <div class=""><?= $error ?></div>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
}
?>