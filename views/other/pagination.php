<?php
if (isset($GLOBALS["paginathion_pages"]) && $GLOBALS["paginathion_pages"]>1) {
    ?>
    <nav aria-label="...">
        <ul class="pagination">
            <?php
            if ($GLOBALS["paginathion_page"] != 1) {
                ?>
                <li class="page-item">
                    <a class="page-link" href="?page=<?= $GLOBALS["paginathion_page"]-1 ?>">Назад</a>
                </li>
                <?php
            }
            ?>
            <?php
            for ($number = 1; $number <= $GLOBALS["paginathion_pages"]; $number++){
                ?>
                <li class="page-item  <?php if ($number == $GLOBALS["paginathion_page"]) echo 'active'; ?>">
                    <?php
                    if ($number == $GLOBALS["paginathion_page"]) {
                        ?>
                        <span class="page-link"><?= $number ?></span>
                        <?php
                    } else {
                        ?>
                        <a class="page-link" href="<?= $GLOBALS["paginathion_link"] ?>?page=<?= $number ?>"><?= $number ?></a>
                        <?php
                    }
                    ?>
                </li>
                <?php
            }
            ?>
            <?php
            if ($GLOBALS["paginathion_page"] != $GLOBALS["paginathion_pages"]) {
                ?>
                <li class="page-item">
                    <a class="page-link" href="?page=<?= $GLOBALS["paginathion_page"]+1 ?>">Вперёд</a>
                </li>
                <?php
            }
            ?>
        </ul>
    </nav>
    <?php
}
?>