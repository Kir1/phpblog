<form role="form" method="post">
    <div class="form-group">
        <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" value="<?php form_old_value("email") ?>" tabindex="1">
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group"><input type="password" name="password" id="password" class="form-control input-lg" placeholder="Пароль" tabindex="2"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-4"><input type="submit" name="submit" value="Отправить" class="btn btn-primary btn-block btn-lg" tabindex="3"></div>
    </div>
</form>
<?php view_rendering('other/errors') ?>