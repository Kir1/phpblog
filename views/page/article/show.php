<div class="text-black-50"><?= $GLOBALS["article"]['created'] ?></div>
<p><?= $GLOBALS["article"]['text'] ?></p>

<?php
if (check_auth()) {
    ?>
    <h3>Добавить комментарий:</h3>
    <?php
    view_rendering('form/comment');
}
?>

<?php
if (count($GLOBALS["comments"])>0) {
    ?>
    <h3>Комментарии:</h3>
    <?php
    foreach ($GLOBALS["comments"] as $comment) {
        ?>
        <div class="p-2 rounded border border-secondary mb-3">
            <a id = "comment<?= $comment['id'] ?>"></a>
            <div class="p-2 rounded border border-secondary mb-3">
                <div class = "text-secondary font-weight-bold"><?= $comment['author'] ?></div>
                <p><?= $comment['text'] ?></p>
            </div>
        </div>
        <?php
    }
    ?>
    <?php
}
?>