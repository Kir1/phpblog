<?php
if (count($GLOBALS["articles"])>0) {
?>
    <?php
    foreach ($GLOBALS["articles"] as $article) {
    ?>
        <div class="p-2 rounded border border-secondary mb-3">
            <a class = "text-secondary font-weight-bold" href="<?= url('articles/'.$article['id']) ?>" title="<?= $article['title'] ?>"><?= $article['title'] ?></a>
            <div class="text-black-50">
                <?= $article['created'] ?>
            </div>
            <div>
                <?= $article['preview'] ?>
            </div>
        </div>
    <?php
    }
    ?>

    <?php view_rendering('other/pagination') ?>
<?php
} else {
?>
    <h2>В данный момент статей нет.</h2>
    <?php
}
?>


