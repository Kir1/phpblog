<html>
<?php view_rendering('main/header'); ?>
<body>
<div class="container">
    <div class="row mt-5 mb-5 bg-info rounded-pill text-white p-3">
        <div class="col text-uppercase font-weight-bold">
            <a class = "text-white" href="/">ПростоБлог</a>
        </div>
        <div class="col text-right text-white">
            <?php
            if (check_auth()) {
                ?>
                <b><?= $_SESSION['user']->email ?></b> | <a class = "text-white" href="<?= url('logout') ?>">Выход</a>
                <?php
            } else {
                ?>
                <a class = "text-white" href="<?= url('login') ?>">Вход</a> | <a class = "text-white" href="<?= url('registration') ?>">Регистрация</a>
                <?php
            }
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <h1><?= $GLOBALS['title'] ?></h1>

            <div class="content">
                <?php view_rendering($GLOBALS['child_view']) ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>