<form role="form" method="post" action="<?= url("comments/create") ?>">
    <a id = "form_create_comment"></a>
    <input type = "hidden" name = "article_id" value = "<?= $GLOBALS["article"]['id'] ?>">
    <div class="form-group">
        <input type="author" name="author" id="author" class="form-control input-lg" placeholder="Ваше имя" value="<?php form_old_value("author") ?>" tabindex="2">
    </div>
    <div class="form-group">

        <textarea type="text" name="text" id="text" class="form-control input-lg" placeholder="Текст" value="<?php form_old_value("text") ?>" tabindex="3" rows="10"></textarea>
    </div>

    <div class="row">
        <div class="col-4"><input type="submit" name="submit" value="Отправить" class="btn btn-primary btn-block btn-lg" tabindex="4"></div>
    </div>
</form>
<?php view_rendering('other/errors') ?>